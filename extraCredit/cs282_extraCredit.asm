#--------------------------------------------------
# cs282_extraCredit.asm
# Author:
#   Andrew Blackton <aabxvc@mail.umkc.edu>
#   Assembler Language Programming - CS282
#   SCE, University of Missouri Kansas City
#
# Revision History:
#   2012.04.10	Andrew Blackton Created
#--------------------------------------------------
        .globl  main
	.data
main.promptUser:	.asciiz "Please enter a string to decode:\n"
        .text
# Register Usage
# $s0: char lineBuffer[104]
# $s1: char outputBuffer[1004]
main:
	# syscall 9 - sbrk (allocate heap memory)
	# $a0: number of bytes to allocate
	# Return $v0 contains address of allocated memory
	li	$a0, 104	# Allocate 104 bytes
	li	$v0, 9		# syscall 9 - sbrk (allocate heap memory)
	syscall			# Allocate space on heap for lineBuffer
	move	$s0, $v0	# initialize &lineBuffer

	# syscall 9 - sbrk (allocate heap memory)
	# $a0: number of bytes to allocate
	# Return $v0 contains address of allocated memory
	li	$a0, 1004	# Allocate 1004 bytes
	li	$v0, 9		# syscall 9 - sbrk (allocate heap memory)
	syscall			# Allocate space on heap for outputBuffer
	move	$s1, $v0	# initialize &outputBuffer

main_loop:
 	# syscall 4 - print string
 	# $a0 : address of null-terminated string to print
 	# Return void
 	la	$a0, main.promptUser
 	li	$v0, 4
 	syscall
	
	# syscall 8 - read string
	# $a0 : output buffer
	# $a1 : max number of characters to read
	# Return void
	li	$v0, 8		# (v0.0) read string
	move	$a0, $s0	# (a0.0) lineBuffer
	li	$a1, 101	# syscall max lineBuffer
	syscall

	lb	$t0, ($s0)		# char evalChar = *lineBuffer
	beq	$t0, '\n', main_exit	# if lineBuffer[0] == '\n' then exit

	move	$a0, $s0	# Initialize decodeString Input Variable - char* lineBuffer
	move	$a1, $s1	# Initialize decodeString Input Variable - char* outputBuffer
	jal	decodeString	# call function decodeString(lineBuffer, outputBuffer)
	move	$t0, $v0	# &endOutputBuffer = char* decodeString(lineBuffer, outputBuffer) 

	move	$a0, $s1		# initialize groupCharactersPrint input char* outputBuffer
	jal	groupCharactersPrint	# call function groupCharactersPrint(&outputBuffer)
	
	b	main_loop

main_exit:
	# syscall 10 - exit (terminate execution)
	# Return void
	li	$v0, 10
	syscall

#----------------------------
# decodeString
# +++++++++++++++++++++++
# Registers
# Input Registers
# $a0: char* lineBuffer
# $a1: char* outputBuffer
# +++++++++++++++++++++++
# $t0: char evalChar
# $t1: int  iterator
# $t8: char* endOutputBuffer
# $t9: char* lineBuffer
#-----------------------------
decodeString:
	move	$t9, $a0
	move	$t8, $a1

decodeString_nextChar:
	lb	$t0, ($t9)	# evalChar = *lineBuffer
	addiu	$t9, $t9, 1	# lineBuffer++
	# if number print next character n+1 times
	beq	$t0, '\n', decodeString_return
	blt	$t0, 0x30, decodeString_notNumber
	bgt	$t0, 0x39, decodeString_notNumber
	# else print character
decodeString_isNumber:
	subiu	$t1, $t0, 0x30	# convert from ASCII to decimal - iterator
	addiu	$t1, $t1, 1	# iterator++
	lb	$t0, ($t9)	# evalChar = *lineBuffer
	addiu	$t9, $t9, 1	# lineBuffer++

decodeString_isNumber_loop:
	beqz	$t1, decodeString_nextChar
	addiu	$t1, $t1, -1	# iterator--
	sb	$t0, ($t8)	# *endOutputBuffer = evalChar
	addiu	$t8, $t8, 1	# endOutputBuffer++
	b	decodeString_isNumber_loop

decodeString_notNumber:
	sb	$t0, ($t8)	# *endOutputBuffer = evalChar
	addiu	$t8, $t8, 1	# endOutputBuffer++
	b	decodeString_nextChar

decodeString_return:
	sb	$0, ($t8)	# *endOutputBuffer = '\0'
	addiu	$t8, $t8, 1	# endOutputBuffer++
	jr	$ra		# return from decodeString function


#-----------------------------
# groupCharactersPrint
# +++++++++++++++++++++++
# Register Usage
# Input Variables
# $a0: char* outputBuffer
# +++++++++++++++++++++++
# $t0: &outputBuffer
# $t7: char evalChar
# $t8: int iterator
# $t9: char groupingCharacter = ' '
#-----------------------------
groupCharactersPrint:
	move	$t0, $a0	# char* outputBuffer
	li	$t9, 0x20	# char groupingCharacter = ' ';
	li	$t8, 0		# int iterator = 3;

groupCharactersPrint_loop:
	lb	$t7, ($t0)	# char evalChar = *lineBuffer;
	addiu	$t0, $t0, 1	# outputBuffer++
	beqz	$t7, groupCharactersPrint_exit
	ble	$t8, 2, groupCharacters_printOutputBuffer
	li	$t8, 0		# iterator = 1 - reset iterator
	move	$a0, $t9	# character to print is ' '
	li	$v0, 11		# syscall 11 - print character
	syscall

groupCharacters_printOutputBuffer:
	addiu	$t8, $t8, 1	# iterator++
	move	$a0, $t7	# character to print is evalChar
	li	$v0, 11		# syscall 11 - print character
	syscall
	b	groupCharactersPrint_loop
		
groupCharactersPrint_exit:
	li	$a0, '\n'	# output ' '
	li	$v0, 11		# syscall 11 - print character
	syscall
	jr	$ra		# return from groupCharacters
