# Begin Data Section
		.data
		.globl main

greet:	.asciiz "Hello, MIPS world!"
# End Data Section
# Begin Text Section
		.text
main:	
		li	$v0, 4		# load syscall value: print string
		la	$a0, greet	# load address of greet into $a0 for syscall
		syscall
		
		li $v0, 10			# graceful exit
		syscall