# Andrew Blackton <aabxvc@mail.umkc.edu>
# Assembler Language Programming - CS282
# University of Missouri Kansas City

 ###############################
#      Register File Usage      #
#      --- --- --- --- ---      #
# Name      Use                 #
# $zero     hardwired 0         #
# $v{0,1}   Function Results/   #
#           Expression Eval     #
# $a[0..3]  Function Arguments  #
# $t[0..9]  Temporaries         #
# $[0-7]    Saved Temporaries   #
# $gp       Global Pointer      #
# $fp       Frame Pointer       #
# $sp       Stack Pointer       #
# $ra       Return Address      #
 ###############################

# Begin Data Section
		.data
		.globl main

greet0:		.asciiz "What is your name? \n"
greet1:		.ascii "Hello, "
name:		.word 0:32
begCount:	.ascii "Your name has "
endCount:	.asciiz " letters."
counter:	.word 0
# End Data Section
# Begin Text Section
		.text

 ###############################
#      Register File Usage      #
#      --- --- --- --- ---      #
# $v0                           #
#                               #
# $a[0..3]                      #
# $t[0..9]                      #
# $[0-7]                        #
# $gp                           #
# $fp                           #
# $sp                           #
# $ra                           #
 ###############################
# Function: main
main:	
		li	$v0, 4		# load syscall value: print string (4)
		la	$a0, greet0	# load address of greet into $a0 for syscall
		syscall
		
		li	$v0, 8		# load syscall value: read string (8)
		li	$a1, 20		# load max value into $a1 for system input
		la	$a0,	name	# load address of input buffer "name" into $a0 for syscall
		syscall
		
		li	$v0, 4		# load syscall value: print string (4)
		la	$a0, greet1	# load address of greet into $a0 for syscall
		syscall
		
		li	$v0, 4		# load syscall value: print string (4)
		la	$a0, name	# load address of greet into $a0 for syscall
		syscall
		
		la	$t0, name	# load address of "name" into $t0
		li	$t1, 0		# initialize value of counter to 0 in $t1


count:		
                lbu	$t2, ($t0)	# load initial character into $t1
		
		addi    $t1, 1          # add 1 to count of letters

		addiu	$t0, t0, 1	# increment memory address
		j loop			# Branch to loop
		
		
		li	$v0, 4		# load syscall value: print string (4)
		la	$a0, begCount	# load address of greet into $a0 for syscall
		syscall
		
		li	$v0, 4		# load syscall value: print string (4)
		la	$a0, endCount	# load address of greet into $a0 for syscall
		syscall
		
		li $v0, 10			# graceful exit
		syscall
