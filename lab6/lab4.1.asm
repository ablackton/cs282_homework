#--------------------------------------------------
# lab4.2.asm
# ********************
# Date* newDate(char month, char day, int year)
#
# struct Date {
# 	char month;	(1 bytes)
# 	char day;	(1 bytes)
# 	int year;	(4 bytes)
# 	Date *next;	(4 bytes)
# }
# Total Size:		(10 bytes) (allocate 12 bytes to keep word aligned)
#
#--------------------------------------------------
.text
main:
	li	$a0, 7
	li	$a1, 4
	li	$a2, 2012
	jal	newDate		# call newDate(7, 4, 2012)
	move	$s0, $v0	# fourth = newDate(7, 4, 2012)

	li	$a0, 12
	li	$a1, 25
	li	$a2, 2012
	jal	newDate		# call newDate(12, 25, 2012)
	move	$s1, $v0	# xmas = newDate(12, 25, 2012)

	sw	$s1, 8($s0)	# fourth->next=xmas;

	move	$s1, $s0
main.loop:
	move	$a0, $s1
	jal	printDate
	
	lw	$s1, 8($s1)
	bnez	$s1, main.loop

exit:
	li	$v0, 10			# (v0.0) terminate execution
	syscall				# graceful termination

# ********************
# Function: newDate
# ********************
# Input Variables
# $a0: char month
# $a1: char day
# $a2: int  year
# ********************
# Register's Used:
#	$t0: address of allocated heap address space
#	$t7: int year
#	$t8: char day
#	$t9: char month
# ********************
newDate:
	# syscall 9 - sbrk (allocate heap memory)
	# $a0 = number of bytes to allocate
	# Return $v0 contains address of allocated memory
	move	$t9, $a0	# month = month
	move	$t8, $a1	# day = day
	move	$t7, $a2	# year = year

	li	$v0, 9		# syscall 9 - sbrk (allocate heap memory)
	li	$a0, 12		# number of bytes to allocate ( 8 bytes will hold entire struct + word align )
	syscall
	move	$t0, $v0	# address of allocated heap address space

	sb	$t9,  ($t0)	# Date.month = month
	sb	$t8, 1($t0)	# Date.day = day
	sw	$t7, 4($t0)	# Date.year = year

	move	$v0, $t0	# Initialize Return - Address of created struct Date
	jr	$ra


.data
slashMe:	.asciiz	"/"
newLine:	.asciiz	"\n"
.text
# ********************
# Function: printDate
# ********************
# Input Variables
# $a0: Date* date
# ********************
printDate:
	lb	$t0,  ($a0)	# month
	lb	$t1, 1($a0)	# day
	lw	$t2, 4($a0)	# year

	# syscall 1 - print integer
	# $a0 = integer to print
	move	$a0, $t0
	li	$v0, 1
	syscall

	# syscall 4 - print string
	# $a0 = address of null-terminated string to print
	la	$a0, slashMe
	li	$v0, 4
	syscall

	# syscall 1 - print integer
	# $a0 = integer to print
	move	$a0, $t1
	li	$v0, 1
	syscall

	# syscall 4 - print string
	# $a0 = address of null-terminated string to print
	la	$a0, slashMe
	li	$v0, 4
	syscall

	# syscall 1 - print integer
	# $a0 = integer to print
	move	$a0, $t2
	li	$v0, 1
	syscall

	# syscall 4 - print string
	# $a0 = address of null-terminated string to print
	la	$a0, newLine
	li	$v0, 4
	syscall

	jr	$ra

