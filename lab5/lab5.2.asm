#--------------------------------------------------
# lab5.2.asm
# Function
# Name: egcd
# ********************
# egcd(a, b)
#   if b == 0: return (1, 0)
#   q = (a / b) // integer division
#   (u, v) = egcd(b, a%b)
#   return (v, u - q*v)
#
# Register's Used:
#   Input Variables
#   $a0: int a
#   $a1: int b
#   Return Variables
#   $v0: v
#   $v0: u - q*v
#--------------------------------------------------
.globl	egcd
.text
egcd:
	addiu	$sp, $sp, -32
	sw	$ra, ($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)

	beqz	$a1, egcd.baseCase	# if b == 0 then return (1,0)
	
	divu	$a0, $a1		# a / b
	mflo	$s0			# (s0) q = a / b
	move	$a0, $a1		# a = b
	mfhi	$a1			# b = a%b
	jal	egcd			# call egcd(b, a%b)
	# Returns
	# $v0: u
	# $v1: v
	multu	$s0, $v1		# q*v
	mflo	$t0			# t0 = q*v
	subu	$t0, $v0, $t0		# t0 = u - q*v
	move	$v0, $v1		# return v
	move	$v1, $t0		# return u - q*v
	b	egcd.clean

egcd.baseCase:
	li	$v0, 1			# return 0: 1
	li	$v1, 0			# return 1: 0
	b	egcd.clean

egcd.clean:
	lw	$ra, ($sp)
	addiu	$sp, $sp, 32
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	jr	$ra
