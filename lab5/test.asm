				.globl main

				.data
prompt1:	.asciiz	"Enter the first integer "
prompt2:	.asciiz	"Enter the second integer "
yes:			.asciiz	"That is correct\n"
no:			.asciiz	"Sorry, that is not correct\n"
				.text				
main:
				li			$v0, 51				# inputDlgInt
				la			$a0, prompt1
				syscall						#status in $a1
												# input in $a0
				beqz		$a1, good			# OK
				beq		$a1, 2, done		# user chose cancel
				b			main					# try again
				
good:		move 	$s1, $a0			# $s1 = a

second:		li			$v0, 51				# inputDlgInt
				la			$a0, prompt1
				syscall						#status in $a1
												# input in $a0
				beqz		$a1, good2		# OK
				beq		$a1, 2, done		# user chose cancel
				b			second				# try again

good2:		move 	$a1, $a0			# $a1 = b	

				move 	$a0, $s0			# $a0 = a
				jal			test
				bnez		$v0, bad
				
				li			$v0, 55				# MessageDialog
				la			$a0, yes			
				li			$a1, 1
				syscall
				b			done
				
bad:
				la			$a0, no
				jal 		error
				
done:		li 			$v0, 10				# Exit
				syscall
				
#**********************************************************
#  test(a, b)
#  	u, v = egcd(a, b)
#     g = gcd(a, b)
#     a*u + b*v - g
#
#**********************************************************
#
#   Register usage
#
#		$s0: a
#    	$s1: b
#    	$s2: u
#		$s3: v
#		$s4: g			

				.data 
big:			.asciiz 	"Overflow!\n"
				.text							
test:
		addiu	$sp, $sp, -24
		sw		$s0, 20($sp)
		sw		$s1, 16($sp)
		sw		$s2, 12($sp)
		sw		$s3,   8($sp)
		sw		$s4,  4($sp)
		sw		$ra,    ($sp)
		jal			egcd
		move	$s2, $v0         # u
		move	$s3, $v1         # v
		move	$a0, $s0         # a
		move	$a1, $s1         # b
		jal			gcd
		move	$s4, $v0         # g
		mul		$t0, $s0, $s2   # t0 <-- a * u
		mfhi		$t1				  # overflow?
		bnez		$t1, over		  # overflow
		mul		$t2, $s1, $s3   # t2 <-- b * v
		mfhi		$t3				  # overflow?
		bnez		$t3, over			# overflow
		add		$t0, $t0, $t1	  # t0  <-- a*u + b*v
		sub		$v0, $t0, $s4	  # v0 -= g
		b			epilog
over:
		la			$a0, big
		jal			error
		
epilog:
		lw		$s0, 20($sp)
		lw		$s1, 16($sp)
		lw		$s2, 12($sp)
		lw		$s3,   8($sp)
		lw		$s4,  4($sp)
		lw		$ra,    ($sp)
		addiu	$sp, $sp, 24
		jr			$ra					

#*****************************************************
#  abort with error message	
#  message address in $a0													
error:				
		li			$v0, 55				# Message Dialog
		li			$a1, 0				# Error
		syscall	
		li			$v0, 10
		syscall
