#--------------------------------------------------
# lab5.1.asm
# Function
# Name: gcd
# ********************
# gcd(a,b){
#   if b==0: return a
#   return gcd(b,a%b)
# }
#
# Register's Used:
#   Input Variables
#   $a0: int a
#   $a1: int b
#   Return Variables
#   $v0: gcd(b, a%b)
#--------------------------------------------------
.globl gcd
.text
gcd:
	addiu	$sp, $sp, -32	# Initialize Area on Stack for Saved Variables
	sw	$ra, ($sp)	# Store Incomming Return Address
	
	beqz	$a1, gcd.retA	# if b == 0: jump to return a
	divu	$a0, $a1
	move	$a0, $a1	# a = b
	mfhi	$a1		# b = a%b
	jal	gcd		# call gcd(b, a%b)
	b	gcd.clean

gcd.retA:
	move	$v0, $a0	# (v0 <- a0) initialize return value a
	b	gcd.clean

gcd.clean:
	lw	$ra, ($sp)
	addiu	$sp, $sp, 32	#
	jr	$ra
