#===============================================================================
# lab2.2.asm
# Author:
#   Andrew Blackton <aabxvc@mail.umkc.edu>
#   Assembler Language Programming - CS282
#   SCE, University of Missouri Kansas City
# 
# Purpose:
#   Prompts the user to enter a string of up to 80 characters, and then print
#   out the number of characters, letters, and digits in the string.
#
# Run Example:
#   Please enter a string: 12 o’clock midnight
#   Your string has 20 characters, 14 letters, and 2 digits 
#
# Revision History:
#   2012.02.02	Updated comments
#   2012.02.01	Andrew Blackton Created
#===============================================================================

		.data
greet:		.asciiz "Please enter a string: "
intro:		.asciiz	"Your string has "
charString:	.asciiz	" characters, "
lettString:	.asciiz	" letters, and "
numbString:	.asciiz	" digits\n"
buffer:		.word	0:10

		.globl	main
		.text
		
#--------------------------------------------------
# Function
# Name: main
# ********************
# Register Usage:
#   $a0: 
#     a0.0: syscall argument values
#     a0.1: isLetter input character
#   $a1: syscall (argument read string) max buffer length
#   $v0:
#     v0.0: syscall service number
#     v0.1: isLetter return value
#   $t0: cursor
#   $t1: characterCount
#   $t2: *cursor
#   $t3: alphaCount
#   $t4: numerCount
#   $t5: 1
#   $t6: 2
#--------------------------------------------------
main:	
		li	  	$v0, 4		# (v0.0) print string
		la		$a0, greet	# (a0.0) input: greet
		syscall				# print(greet)
		
		li		$v0, 8		# (v0.0) read string
		la		$a0, buffer	# (a0.0) output: buffer
		li		$a1, 81		# syscall max buffer length
		syscall
		
		li		$t1, 0		# int characterCount = 0
		li		$t3, 0		# int letterCount = 0
		li		$t4, 0		# int numberCount = 0
		li		$t5, 1		# int 1 for comparison
		li		$t6, 2		# int 2 for comparison
		
		la		$t0, buffer	# cursor = buffer
		lbu		$t2, ($t0)	# t2 <-- *cursor


loop:		beqz		$t2, done	# while *cursor != 0

		move		$a0, $t2	# (a0.1) alphaNum(*cursor)
		jal		alphaNum	# call alphaNum(*cursor)

		beq		$t5, $v0, letCount
		beq		$t6, $v0, numCount
		beqz		$v0, cont

letCount:	addi		$t3, $t3, 1	# increment letter count
		b		cont
numCount:	addi		$t4, $t4, 1	# increment number count
		b		cont

cont:
		addi		$t1, $t1, 1	# characterCount++
		addiu		$t0, $t0, 1	# cursor++
		lbu		$t2, ($t0)	# t2 <-- *cursor
		b		loop			
		
done:		li	  	$v0, 4		# (v0.0) print string
		la		$a0, intro	# (a0.0) input: intro
		syscall				# print(intro)
		
		li		$v0, 1		# (v0.0) print int
		move		$a0, $t1	# (a0.0) input: characterCount
		syscall				# print(characterCount)
		
		li	  	$v0, 4		# (v0.0) print string
		la		$a0, charString	# (a0.0) input: charString
		syscall				# print(charString)
		
		li		$v0, 1		# (v0.0) print int
		move		$a0, $t3	# (a0.0) input: alphaCount
		syscall				# print(alphaCount)
		
		li	  	$v0, 4		# (v0.0) print string
		la		$a0, lettString	# (a0.0) input: lettString
		syscall				# print(lettString)
		
		li		$v0, 1		# (v0.0) print int
		move		$a0, $t4	# (a0.0) input: numerCount
		syscall				# print(numerCount)
		
		li	  	$v0, 4		# (v0.0) print string
		la		$a0, numbString	# (a0.0) input: numbString
		syscall				# print(numbString)
		
		li		$v0, 10		# (v0.0) terminate execution
		syscall				# graceful termination

#--------------------------------------------------
# Function
# Name: alphaNum
# ********************
# Purpose: Takes one character as an input parameter and return 1 if that
# character is a letter, 2 if the character is a digit, and 0 for anything else.
# ********************
# Register Usage:
#   $a0: input character
#   $v0: return(2||1||0)
#--------------------------------------------------
alphaNum:
		blt		$a0, '0', nothing	# not a number if < '0'
		ble		$a0, '9', number	# if <= '9' a number
		blt		$a0, 'A', nothing	# not a letter if < 'A'
		ble		$a0, 'Z', alpha		# if <= 'Z' it's a letter
		blt		$a0, 'a', nothing	# not a letter if < 'a'
		ble		$a0, 'z', alpha		# if <= 'z' it's a letter
		b		nothing			# not a letter if we get here
		
number:		li		$v0, 2		# return 2 if a number
		jr		$ra
alpha:		li		$v0, 1		# return 1 if a letter
		jr		$ra
nothing:	li		$v0, 0		# return 0 if not a letter or number
		jr		$ra
