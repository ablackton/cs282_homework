#===============================================================================
# lab2.1.asm
# Author:
#   Andrew Blackton <aabxvc@mail.umkc.edu>
#   Assembler Language Programming - CS282
#   SCE, University of Missouri Kansas City
# 
# Purpose:
#   Ask a user their name and count number of letters entered.
#
# Run Example:
#   What is your name? Andrew Blackton
#   Your name has 14 letters.
# 
# Revision History:
#   2012.02.02	Updated comments
#   2012.02.01	Andrew Blackton Created
#===============================================================================

		.data
greet:		.asciiz "What is your name? "
answer1:	.asciiz	"Your name has "
answer2:	.asciiz	" letters.\n"
buffer:		.word	0:10

		.globl	main
		.text
		
#--------------------------------------------------
# Function
# Name: main
# ********************
# Register Usage:
#   $a0: 
#     a0.0: syscall argument values
#     a0.1: isLetter input character
#   $a1: syscall (argument read string) max buffer length
#   $v0:
#     v0.0: syscall service number
#     v0.1: isLetter return value
#   $t0: cursor
#   $t1: count
#   $t2: *cursor
#--------------------------------------------------
main:	
		li	  	$v0, 4		# (v0.0) print string
		la		$a0, greet	# (a0.0) input: greet
		syscall				# print(greet)
		
		li		$v0, 8		# (v0.0) read string
		la		$a0, buffer	# (a0.0) output buffer
		li		$a1, 81		# syscall max buffer length
		syscall
		
		li		$t1, 0		# count = 0
		la		$t0, buffer	# cursor = buffer
		lbu		$t2, ($t0)	# t2 <-- *cursor
		
	miter:	beqz		$t2, mdone	# while *cursor != 0

		move		$a0, $t2	# (a0.1) isLetter(*cursor)
		jal		isLetter	# call isLetter(*cursor)
		add		$t1, $t1, $v0	# (v0.1) count += isLetter(*cursor)

		addiu		$t0, $t0, 1	# cursor++
		lbu		$t2, ($t0)	# t2 <-- *cursor
		b		miter		# loop
		
	mdone:	li	  	$v0, 4		# (v0.0) print string
		la		$a0, answer1	# (a0.0) input: answer1
		syscall				# print(answer1)
		
		li		$v0, 1		# (v0.0) print int
		move		$a0, $t1	# (a0.0) input: count
		syscall				# print(count)
		
		li	  	$v0, 4		# print string
		la		$a0, answer2	# string to print
		syscall				# print(answer2)
		
		li		$v0, 10		# (v0.0) terminate execution
		syscall				# graceful termination
		
		
#--------------------------------------------------
# Function
# Name: isLetter
# ********************
# Purpose: Take an input parameter (a character) and return 1 if that character 
# is a letter and return 0 if it is not a letter.
# ********************
# Register Usage:
#   $a0: input character
#   $v0: return(1||0)
#--------------------------------------------------
isLetter:
		blt		$a0, 'A', dont	# not a letter if < 'A'
		ble		$a0, 'Z', count	# if <= 'Z' it's a letter
		blt		$a0, 'a', dont	# not a letter if < 'a'
		ble		$a0, 'z', count	# if <= 'z' it's a letter
		b		dont		# not a letter if we get here
		
	count:	li		$v0, 1		# return 1 if a letter
		jr		$ra
	dont:	li		$v0, 0		# return 0 if not a letter
		jr		$ra
